# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from block.tests.factories import SectionFactory
from compose.models import SECTION_BODY, SECTION_CARD, SECTION_SLIDESHOW


@pytest.mark.django_db
def test_init_project():
    call_command("init_app_compose")
    call_command("init_project")
