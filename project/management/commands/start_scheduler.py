# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from pytz import utc

from base.scheduler_utils import create_scheduler


class Command(BaseCommand):
    """Start APScheduler scheduler."""

    help = "Start APScheduler..."

    def handle(self, *args, **options):
        self.stdout.write("{}...".format(self.help))
        scheduler = create_scheduler()
        self.stdout.write("Scheduler, adding jobs...")
        # process_mail
        scheduler.add_job(
            "mail.tasks:schedule_process_mail",
            "interval",
            minutes=60,
            id="schedule_process_mail",
            max_instances=1,
            replace_existing=True,
        )
        self.stdout.write("Scheduler, starting...")
        scheduler.start()
        self.stdout.write("{} - Complete".format(self.help))
