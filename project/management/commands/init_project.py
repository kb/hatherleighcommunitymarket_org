# -*- encoding: utf-8 -*-
"""
This command is designed to be run multiple times.  It will clear out data, and
then re-insert e.g. for setting up the main menu navigation.
"""
from django.core.management import call_command
from django.core.management.base import BaseCommand

from block.models import (
    Page,
    PageSection,
    Section,
    Template,
    TemplateSection,
    Url,
)
from compose.models import SECTION_BODY, SECTION_CARD, SECTION_SLIDESHOW


class Command(BaseCommand):

    help = "Init project"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        self.stdout.write("{} - Complete".format(self.help))
