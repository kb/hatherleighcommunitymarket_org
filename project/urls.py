# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.sitemaps import GenericSitemap
from django.contrib.sitemaps.views import sitemap
from django.views.generic import RedirectView

from block.models import Page
from web.views import EnquiryCreateView
from django.urls import path, re_path, include

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls


info_dict = {"queryset": Page.objects.pages(), "date_field": "modified"}

sitemaps = {
    "block": GenericSitemap(info_dict, priority=0.5, changefreq="monthly")
}


urlpatterns = [
    url(regex=r"^dash/", view=include("dash.urls")),
    url(regex=r"^dash/enquiry/", view=include("enquiry.urls")),
    url(regex=r"^dash/gdpr/", view=include("gdpr.urls")),
    url(regex=r"^dash/mail/", view=include("mail.urls")),
    url(
        regex=r"^dash/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="project.dash",
    ),
    url(
        regex=r"^dash/home/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="project.home",
    ),
    url(
        regex=r"^dash/oidc/not/used/$",
        view=RedirectView.as_view(
            pattern_name="project.settings", permanent=False
        ),
        name="oidc_authentication_init",
    ),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    re_path(r"", include(wagtail_urls)),
    url(
        regex=r"^sitemap\.xml$",
        view=sitemap,
        kwargs={"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    url(regex=r"^", view=include("login.urls")),
    # url(regex=r"^block/", view=include("block.urls.block")),
    # url(regex=r"^compose/", view=include("compose.urls.compose")),
    # url(
    #     regex=r"^contact/$",
    #     view=EnquiryCreateView.as_view(),
    #     kwargs=dict(page=Page.CUSTOM, menu="contact"),
    #     name="web.contact",
    # ),
    # url(regex=r"^enquiry/", view=include("enquiry.urls")),
    # url(regex=r"^gallery/", view=include("gallery.urls")),
    # url(regex=r"^gdpr/", view=include("gdpr.urls")),
    # url(regex=r"^mail/", view=include("mail.urls")),
    # url(regex=r"^wizard/", view=include("block.urls.wizard")),
    # # this url include should come last
    # url(regex=r"^", view=include("block.urls.cms")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        url(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
