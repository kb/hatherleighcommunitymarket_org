#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

DB_NAME="dev_www_hatherleighcommunitymarket_org_`id -nu`"
psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DB_NAME};"
psql -X -U postgres -c "CREATE DATABASE ${DB_NAME} TEMPLATE=template0 ENCODING='utf-8';"

django-admin.py migrate --noinput
django-admin.py demo_data_login
django-admin.py init_app_block
django-admin.py init_app_compose
django-admin.py init_project
django-admin.py runserver 0.0.0.0:8000
