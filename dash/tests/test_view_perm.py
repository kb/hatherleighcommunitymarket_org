# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_settings(perm_check):
    url = reverse("project.settings")
    perm_check.staff(url)
