# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url_name",
    [
        "project.dash",
        "project.home",
        "oidc_authentication_init",
    ],
)
def test_dash_redirect(client, url_name):
    response = client.get(reverse(url_name))
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.settings") == response.url
