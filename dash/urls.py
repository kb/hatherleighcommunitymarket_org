# -*- encoding: utf-8 -*-
from django.conf.urls import url

from .views import SettingsView


urlpatterns = [
    url(
        regex=r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
]
