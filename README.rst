hatherleighcommunitymarket_org
******************************

.. warning:: Replaced by https://gitlab.com/kb/simple-site

Development
===========

Install
-------

Virtual Environment
-------------------

If you've got dev-scripts installed type::

  create-venv hatherleighcommunitymarket-org

Otherwise::

  python3 -m venv venv-hatherleighcommunitymarket-org
  source venv-hatherleighcommunitymarket-org/bin/activate
  pip install --upgrade pip

  pip install -r requirements/local.txt

Testing
-------

::

  find . -name '*.pyc' -delete
  pytest -x

Create a development environment
--------------------------------

To configure a blank development environment use::

  ./init_dev.sh

Release and Deploy
==================

https://www.kbsoftware.co.uk/docs/


djhtml
------

::

  djhtml -t 2 -i template.html

tailwind
--------

From
https://tailwindcss.com/docs/installation#installing-tailwind-css-as-a-post-css-plugin

::

  cd front
  npm install -D tailwindcss@latest postcss@latest autoprefixer@latest postcss-cli@latest
  # run the 'build:css' script from 'package.json'
  npm run build:css
