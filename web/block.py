# -*- encoding: utf-8 -*-
from typing import Text
from wagtail.core.blocks.field_block import (
    DateBlock,
    PageChooserBlock,
    RichTextBlock,
    TextBlock,
)
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core import blocks
from wagtail.core.blocks import StreamBlock
from wagtail.core.fields import RichTextField


class HeroBlock(blocks.StructBlock):
    """Hero block."""

    title = blocks.CharBlock(required=True, help_text="Main hero title")
    subtitle = blocks.CharBlock(
        required=False,
        help_text="Small text below main title, (leave blank to hide)",
    )
    description = RichTextBlock(
        required=True, help_text="Description about the site / company"
    )
    image = ImageChooserBlock(required=True, help_text="Organisation image")
    button_1 = PageChooserBlock(
        required=False, help_text="Page Redirect (leave blank to hide)"
    )
    button_1_name = blocks.CharBlock(required=False, help_text="Text on button")
    button_2 = PageChooserBlock(
        required=False,
        help_text="Page 2 Redirect, Requires Link 1 (leave blank to hide)",
    )
    button_2_name = blocks.CharBlock(required=False, help_text="Text on button")

    class Meta:
        icon = "grip"
        template = "web/hero.html"


class TextImageBlock(blocks.StructBlock):
    """Content block."""

    title = blocks.CharBlock(required=True, help_text="Main Image block Title")
    subtitle = blocks.CharBlock(
        required=False, help_text="Subtitle below title, (Leave blank to hide)"
    )
    introduction = RichTextBlock(
        required=True,
        help_text="Main Introduction (small bit of text introducing the post)",
    )
    content = RichTextBlock(
        required=True, help_text="Main description for the block"
    )
    image = ImageChooserBlock(required=True, help_text="Main image")
    image_credit = blocks.CharBlock(
        required=False,
        help_text="image Credit (Credit the imagegrapher, leave blank to hide)",
    )

    class Meta:
        icon = "image"
        template = "web/text_image.html"


class TwoColumnTextBlock(blocks.StructBlock):
    """Two column text block."""

    title = blocks.CharBlock(required=True, help_text="Main Text Title")
    subtitle = blocks.CharBlock(
        required=False, help_text="Small subtitle (Leave blank to hide)"
    )
    column_1 = RichTextBlock(
        required=True, help_text="Column 1 of Text (Left Side)"
    )
    column_2 = RichTextBlock(
        required=True, help_text="Column 2 of Text (Right Side)"
    )

    class Meta:
        icon = "doc-full"
        template = "web/two_column_text.html"


class GalleryBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        required=True, max_length=40, help_text="Title of the Gallery section"
    )
    description = TextBlock(
        required=True,
        max_length=300,
        help_text="Small description of the gallery",
    )

    images = blocks.ListBlock(
        blocks.StructBlock(
            [
                (
                    "image",
                    ImageChooserBlock(required=True, help_text="Gallery Image"),
                ),
                (
                    "title",
                    blocks.CharBlock(
                        required=True,
                        help_text="Text under the image",
                        max_length=40,
                    ),
                ),
                (
                    "description",
                    blocks.TextBlock(
                        required=True,
                        help_text="Blue text underneath the title, small description.",
                        max_length=80,
                    ),
                ),
            ]
        )
    )

    class Meta:
        icon = "doc-full"
        template = "web/gallery.html"


class DocumentBlock(blocks.StructBlock):
    """Two column text block."""

    title = blocks.CharBlock(required=True)
    description = RichTextBlock(required=True)
    cards = blocks.ListBlock(
        blocks.StructBlock(
            [
                (
                    "date",
                    DateBlock(
                        required=True, help_text="Date the document was updated"
                    ),
                ),
                (
                    "title",
                    blocks.CharBlock(
                        required=True,
                        max_length=30,
                        help_text="Title of Document",
                    ),
                ),
                (
                    "subtitle",
                    blocks.CharBlock(
                        required=False,
                        max_length=30,
                        help_text="Piece of small text above the main title (if empty, will be blank)",
                    ),
                ),
                (
                    "image",
                    ImageChooserBlock(
                        required=True, help_text="Image above document"
                    ),
                ),
                (
                    "description",
                    TextBlock(
                        required=True,
                        max_length=400,
                        help_text="Document Description",
                    ),
                ),
                (
                    "document",
                    DocumentChooserBlock(
                        required=True, help_text="Downloadable Document"
                    ),
                ),
            ]
        )
    )

    class Meta:
        icon = "doc-full"
        template = "web/documents.html"
