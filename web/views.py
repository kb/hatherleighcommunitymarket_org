# -*- encoding: utf-8 -*-
from django.views.generic import CreateView

from block.models import Page
from block.views import CmsMixin, ContentPageMixin, PageMixin, PageTemplateMixin
from enquiry.views import EnquiryCreateMixin


class EnquiryCreateView(
    PageMixin,
    PageTemplateMixin,
    EnquiryCreateMixin,
    ContentPageMixin,
    CmsMixin,
    CreateView,
):
    """Save an enquiry in the database."""

    def get_success_url(self):
        page = Page.objects.get(slug="contact", slug_menu="thankyou")
        return page.get_absolute_url()
