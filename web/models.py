# -*- encoding: utf-8 -*-
from django.db import models
from django.db.models.fields import CharField
from django.shortcuts import render
from wagtail.admin.edit_handlers import (
    FieldPanel,
    MultiFieldPanel,
    PageChooserPanel,
    StreamFieldPanel,
)
from gallery.models import Image
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from enquiry.forms import EnquiryForm
from enquiry.models import Enquiry
from gdpr.models import Consent
from .block import (
    DocumentBlock,
    GalleryBlock,
    HeroBlock,
    TextImageBlock,
    TwoColumnTextBlock,
)


SECTION_BODY = "body"
SECTION_CARD = "card"
SECTION_GALLERY = "gallery"
SECTION_NEWS = "news"
SECTION_SLIDESHOW = "slideshow"


class MenuContext:
    def get_context(self, request):
        context = super().get_context(request)
        root_page = self.get_site().root_page
        context["home_page"] = root_page
        context["site_menu"] = root_page.get_children().filter(
            live=True, show_in_menus=True
        )
        return context


class ContactPage(MenuContext, Page):
    message = models.CharField(
        max_length=50,
        null=True,
        help_text="Message above contact form (`Your Contact Info Here` etc)",
    )
    card_title = models.CharField(
        max_length=50,
        null=True,
        help_text="Text above contact information",
    )
    card_description = models.CharField(
        max_length=300,
        null=True,
        help_text="Description of contact information (Why / When to contact etc)",
    )
    phone_number = models.CharField(
        max_length=50,
        null=True,
        help_text="Phone number (e.g +44 7........)",
    )
    email = models.CharField(
        max_length=50,
        null=True,
        help_text="Email address (e.g ryan....@email.com)",
    )
    thankyou_title = models.CharField(
        max_length=150,
        null=True,
        help_text="Description after contact form submission",
    )
    thankyou_message = models.CharField(
        max_length=50,
        null=True,
        help_text="Thank you message after form submission",
    )
    page_name = models.CharField(
        max_length=50, null=True, help_text="Page name on button"
    )
    page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Go to this page after clicking the button",
    )

    content_panels = Page.content_panels + [
        FieldPanel("message"),
        FieldPanel("card_title"),
        FieldPanel("card_description"),
        FieldPanel("phone_number"),
        FieldPanel("email"),
        FieldPanel("thankyou_title"),
        FieldPanel("thankyou_message"),
        FieldPanel("page_name"),
        PageChooserPanel("page"),
    ]

    def _consent(self):
        return Consent.objects.get_consent(Enquiry.GDPR_CONTACT_SLUG)

    def serve(self, request):
        context = super().get_context(request)
        kwargs = dict(consent=self._consent(), request=request)
        if request.method == "POST":
            form = EnquiryForm(request.POST, **kwargs)
            if form.is_valid():
                enquiry = form.save()
                context.update({"enquiry": enquiry, "page": self})
                return render(request, self.template, context)
        else:
            form = EnquiryForm(**kwargs)
        context.update({"enquiry": None, "form": form, "page": self})
        return render(request, self.template, context)

    max_count = 1


class DefaultPage(MenuContext, Page):
    body = StreamField(
        [
            ("hero", HeroBlock()),
            ("text_image", TextImageBlock()),
            ("two_column_text", TwoColumnTextBlock()),
            ("Documents", DocumentBlock()),
            ("Gallery", GalleryBlock()),
        ]
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel("body"),
    ]


@register_setting
class FooterSettings(BaseSetting):
    """Social media settings for our custom website."""

    facebook = models.URLField(
        blank=True,
        null=True,
        help_text="Facebook URL. Leave blank to remove it from the footer.",
    )
    instagram = models.URLField(
        blank=True,
        null=True,
        help_text="Instagram URL. Leave blank to remove it from the footer.",
    )
    linkedin = models.URLField(
        blank=True,
        null=True,
        help_text="Linkedin URL. Leave blank to remove it from the footer.",
    )
    twitter = models.URLField(
        blank=True,
        null=True,
        help_text="Twitter URL. Leave blank to remove it from the footer.",
    )
    company_name = models.CharField(
        blank=True,
        max_length=30,
        null=True,
        help_text="Your Company name Here!",
    )

    panels = [
        MultiFieldPanel(
            [
                FieldPanel("facebook"),
                FieldPanel("instagram"),
                FieldPanel("linkedin"),
                FieldPanel("twitter"),
                FieldPanel("company_name"),
            ]
        )
    ]
