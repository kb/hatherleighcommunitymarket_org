# -*- encoding: utf-8 -*-
# import pytest
#
# from http import HTTPStatus
#
# from block.tests.factories import PageFactory, TemplateFactory
# from enquiry.models import Enquiry
# from gdpr.tests.factories import ConsentFactory
# from login.tests.factories import UserFactory
# from mail.models import Mail
# from mail.tests.factories import NotifyFactory
#
#
# @pytest.mark.django_db
# def test_contact(client):
#    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
#    NotifyFactory(email="test@pkimber.net")
#    # action
#    response = client.post(
#        page_contact.get_absolute_url(),
#        {
#            "name": "Richard",
#            "description": "Do you sell hay and straw?",
#            "email": "richard@pkimber.net",
#            "phone": "0000",
#            "consent_checkbox": True,
#            "g-recaptcha-response": "PASSED",
#        },
#    )
#    url = page_thankyou.get_absolute_url()
#    assert response.status_code == HTTPStatus.FOUND, response.context[
#        "form"
#    ].errors
#    assert url in response["Location"]
#    enquiry = Enquiry.objects.first()
#    assert enquiry.email == "richard@pkimber.net"
#    assert enquiry.description == "Do you sell hay and straw?"
#    mail = Mail.objects.first()
#    assert mail.message.subject == "Enquiry from Richard"
#
#
# @pytest.mark.django_db
# def test_logged_in_contact(client):
#    ConsentFactory(slug=Enquiry.GDPR_CONTACT_SLUG)
#    user = UserFactory(
#        email="john@pkimber.net", first_name="john", last_name="doe"
#    )
#    client.force_login(user)
#    NotifyFactory(email="test@pkimber.net")
#    # action
#    response = client.get(page_contact.get_absolute_url())
#    # assert response.context['form'].fields['name'].initial == 'john doe'
#    response = client.post(
#        page_contact.get_absolute_url(),
#        {
#            "name": "john doe",
#            "description": "Do you sell hay and straw?",
#            "email": "john@pkimber.net",
#            "phone": "0000",
#            "consent_checkbox": True,
#            "g-recaptcha-response": "PASSED",
#        },
#    )
#    url = page_thankyou.get_absolute_url()
#    assert response.status_code == HTTPStatus.FOUND, response.context[
#        "form"
#    ].errors
#    assert url in response["Location"]
#    enquiry = Enquiry.objects.first()
#    assert enquiry.email == "john@pkimber.net"
#    assert enquiry.description == "Do you sell hay and straw?"
#    mail = Mail.objects.first()
#    assert mail.message.subject == "Enquiry from john doe"
