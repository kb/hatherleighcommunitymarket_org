# -*- encoding: utf-8 -*-

from enquiry.forms import EnquiryForm


class BeaconEnquiryForm(EnquiryForm):
    def __init__(self, *args, **kwargs):
        self.page = kwargs.pop("page")
        super().__init__(*args, **kwargs)

    def _email_subject(self, instance):
        return "'{}' Enquiry from {}".format(self.page.name, instance.name)
