# -*- encoding: utf-8 -*-
from .base import *

DEBUG = False
THUMBNAIL_DEBUG = DEBUG
TESTING = get_env_variable_bool("TESTING")

if get_env_variable_bool("SSL"):
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

ALLOWED_HOSTS = [get_env_variable("ALLOWED_HOSTS")]


DOMAIN = get_env_variable("DOMAIN")
DATABASE_NAME = DOMAIN.replace(".", "_").replace("-", "_")
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DATABASE_NAME,
        "USER": DATABASE_NAME,
        "PASSWORD": get_env_variable("DB_PASS"),
        "HOST": get_env_variable("DB_IP"),
        "PORT": "",
    }
}

FTP_STATIC_DIR = None
FTP_STATIC_URL = None

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = get_env_variable("MEDIA_ROOT")

# https://github.com/johnsensible/django-sendfile
SENDFILE_BACKEND = "sendfile.backends.nginx"
SENDFILE_ROOT = get_env_variable("SENDFILE_ROOT")
SENDFILE_URL = "/private"

NOCAPTCHA = True
RECAPTCHA_PRIVATE_KEY = get_env_variable("NORECAPTCHA_SECRET_KEY")
RECAPTCHA_PUBLIC_KEY = get_env_variable("NORECAPTCHA_SITE_KEY")

REDIS_HOST = get_env_variable("REDIS_HOST")
REDIS_PORT = get_env_variable("REDIS_PORT")
# https://dramatiq.io/reference.html#middleware
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {"url": "redis://{}:{}/0".format(REDIS_HOST, REDIS_PORT)},
    "MIDDLEWARE": [
        # drops messages that have been in the queue for too long
        "dramatiq.middleware.AgeLimit",
        # cancels actors that run for too long
        "dramatiq.middleware.TimeLimit",
        # lets you chain success and failure callbacks
        "dramatiq.middleware.Callbacks",
        # automatically retries failed tasks with exponential backoff
        "dramatiq.middleware.Retries",
    ],
}
# KB Software queue name (to allow multiple sites on one server)
DRAMATIQ_QUEUE_NAME = DATABASE_NAME
DRAMATIQ_QUEUE_NAME_PIPELINE = "{}_pipeline".format(DATABASE_NAME)
