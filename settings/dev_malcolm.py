# -*- encoding: utf-8 -*-
import socket
from kombu import Exchange, Queue
from .local import *
from celery.schedules import crontab

DATABASE = "dev_www_hatherleighcommunitymarket_org_malcolm"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DATABASE,
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}

# Celery
# transport
BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/0"
# number of worker processes (will be 3 == controller, worker and beat)
CELERYD_CONCURRENCY = 1
# rate limits
CELERY_DISABLE_RATE_LIMITS = True
# serializer
CELERY_TASK_SERIALIZER = "json"
CELERY_ACCEPT_CONTENT = ["json"]
# queue
CELERY_DEFAULT_QUEUE = DATABASE
CELERY_QUEUES = (Queue(DATABASE, Exchange(DATABASE), routing_key=DATABASE),)

CELERYBEAT_SCHEDULE = {
    "process_mail": {"task": "mail.tasks.process_mail", "schedule": crontab()}
}

INTERNAL_IPS = ["127.0.0.1"]

ALLOWED_HOSTS = [
    "localhost",
    "127.0.0.1",
    "localhost",
    "::1",
    socket.gethostbyname(socket.gethostname()),
]
